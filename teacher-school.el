;;; teacher-school.el --- A package to manage school information for teachers.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience, extensions, files, data, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 


;;; Code:

(require 'teacher)


;;; Variables

(defcustom teacher-schools []
  "A list of strings containing the schools' directory names."
  :type '(array string)
  :group 'teacher)

(defcustom teacher-school-context-prompt "Select new school's context (%s): "
  "The prompt to show when asking for context."
  :type 'string
  :group 'teacher)

(defcustom teacher-school-create-context-directory-prompt
  "Are you sure you want to create the context directory for '%s'?"
  "The propt to show when asking for confirmation for creating a
context directory."
  :type 'string
  :group 'teacher)

(defcustom teacher-school-periods '()
  "A plist for registering the number of periods for each school."
  :type '(plist :key-type string :value-type natnum)
  :group 'teacher)

(defcustom teacher-school-current-periods '()
  "A plist for registering current period for each school."
  :type '(plist :key-type string :value-type natnum)
  :group 'teacher)

(defcustom teacher-school-period-names '()
  "The name of the periods on each school."
  :type '(plist :key-type string :value-type string)
  :group 'teacher)

(defvar teacher-school-context nil
  "Sets the school context to use by teacher.")

(defvar teacher-school--data nil
  "A plist containing the properties of the school.")


;;; Functions

(defun teacher-school-contexts ()
  (append teacher-schools nil))

(defun teacher-school-set-context (&optional school)
  "Sets the new school context."
  (setq teacher-school-context school))
;; (teacher-school-set-context nil)

(defun teacher-school-get-context ()
  "Returns a valid context.

If the value of `teacher-school-context' is nil, and
`teacher-schools' have any members, it will prompt for a vaild
school, set that value to `teacher-school-context' and return
that value."
  (when (and teacher-school-context
             (not (member teacher-school-context
                          (teacher-school-contexts)))
             (teacher-school-contexts))
    (teacher-school-select-context))
  teacher-school-context)
;; (teacher-school-get-context) (setq teacher-school-context "X")

(defun teacher-school-directory ()
  "Returns the path to the school directory."
  (file-name-as-directory
   (expand-file-name
    (or (teacher-school-get-context)
        "")
    teacher-directory)))
;; (teacher-school-directory)

(defun teacher-school-lective-year-directory ()
  "Returns the path to the current lective-year directory.

It will be sure that it is within a valid school context."
  (file-name-as-directory
   (expand-file-name
    (teacher-get-lective-year)
    (teacher-school-directory))))
;; (teacher-school-lective-year-directory)

(defun teacher-school-lective-year-prev-directory ()
  "Returns the path to the previous lective-year directory.

It will be sure that it is within a valid school context."
  (file-name-as-directory
   (expand-file-name
    (teacher-get-lective-year-prev)
    (teacher-school-directory))))
;; (teacher-school-lective-year-directory)

(defun teacher-school-lective-year-next-directory ()
  "Returns the path to the next lective-year directory.

It will be sure that it is within a valid school context."
  (file-name-as-directory
   (expand-file-name
    (teacher-get-lective-year-next)
    (teacher-school-directory))))
;; (teacher-school-lective-year-directory)

(defun teacher-school-get-current-period ()
  (plist-get teacher-school-current-periods
                 (or teacher-school-context "")))

(defun teacher-school-period-directory ()
  "Returns the path to the current context diretory."
  (file-name-as-directory
   (expand-file-name
    (if (teacher-school-get-current-period)
        (concat (or (plist-get teacher-school-period-names (teacher-school-get-context)) "Period")
                (teacher-school-get-current-period))
      "")
    (teacher-school-lective-year-directory))))

;;;###autoload
(defun teacher-school--init ()
  "Creates the school most needed directories."
  (teacher--create-directory-no-prompt
   (teacher-school-period-directory))
  (teacher--create-directory-no-prompt
   (expand-file-name "students" (teacher-school-directory)))
  (teacher--create-directory-no-prompt
   (expand-file-name "papers/works" (teacher-school-directory))))


;;; Commands

;;;###autoload
(defun teacher-school-select-context ()
  "Selects the new context from the list of candidates (`teacher-schools').

If the list of candidates is empty, it will not change it's
value.

Returns the (possibly) new value of `teacher-school-context'."
  (interactive)
  (when (teacher-school-contexts)
    (teacher-school-set-context
     (completing-read (format teacher-school-context-prompt
                              teacher-school-context)
                      (teacher-school-contexts))))
  teacher-school-context)

;;;###autoload
(defun teacher-school-unset-context ()
  "Unsets the school context."
  (interactive)
  (teacher-school-set-context nil))

(provide 'teacher-school)
;;; teacher-school.el ends here
