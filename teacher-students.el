;;; teacher-students.el --- Paquete para manejar la lista de alumnos de un maestro.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author:           Alejandro Barocio A. <abarocio80@gmail.com>
;; Filename:         teacher-students.el
;; Keywords:         extensions, convenience, files, data, tools
;; Package-Requires: ((emacs "27.1") (csv "2.1"))
;; Version:          0.1.0

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'teacher-school)
(require 'csv) ;; (crafted-package-install-package 'csv)


;;; Variables

(defcustom teacher-students-plist-file "students.eld"
  "Location of the data file containing the students information."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-csv-file "students.csv"
  "Location of the data file containing the students information."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-csv-file-headers
  '("ID" "Group" "Rol number" "Last name" "First name" "Email")
  "The headers in the CSV file first row.

If empty, we will suppose that the file has no headers."
  :type '(list (string :tag "ID")
               (string :tag "Group")
               (string :tag "Rol number")
               (string :tag "Last name")
               (string :tag "First name")
               (string :tag "Email"))
  :group 'teacher)

(defcustom teacher-students-id-prompt "ID: "
  "The prompt to ask for the student's ID."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-gr-prompt "Group: "
  "The prompt to ask for the student's group."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-rn-prompt "Roll number: "
  "The prompt to ask for the student's roll number."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-ln-prompt "Last name: "
  "The prompt to ask for the student's Last name."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-fn-prompt "First name: "
  "The prompt to ask for the student's First name."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-ml-prompt "Email: "
  "The prompt to ask for the student's email address."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-eld-file-not-exists-msg
  "The ELD file doesn't exists: `%s'."
  "The message to display when the ELD file for the context doesn't
exists."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-csv-insufficient-data-msg
  "There are fields empty that must not be empty.
   At least you must set the following fields in your CSV:
   - %s
   - %s
   - %s"
  "The parameterized string to display error when processing the
CSV file and any of the three required fields (ID, first name and
last name) are empty."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-csv-file-not-exists-msg
  "The CSV file doesn't exists: `%s'."
  "The message to display when the CSV file for the context doesn't
exists."
  :type 'string
  :group 'teacher)

(defcustom teacher-students-csv-no-data-msg
  "There is no data for students in file `%s'."
  "The message to display when the CSV file is empty."
  :type 'string
  :group 'teacher)

(defvar teacher-students--plist nil
  "The data of the students.")

(defvar teacher-students--parameters-index
  '(:id 0 :gr 1 :rn 2 :ln 3 :fn 4 :ml 5)
  "A plist indicating the expected order of fields in
`teacher-students-csv-file-headers' or in CSV files without
headers.")


;;; Functions

(defun teacher-students-plist-file (&optional fn)
  "Returns the path to the plist-file for the context.

If FN is non-nil, it will use `teacher-students-plist-file',
otherwise use the string value of FN."
  (expand-file-name (or fn teacher-students-plist-file)
                    (teacher-school-lective-year-directory)))

(defun teacher-students-csv-file (&optional fn)
  "Returns the path to the plist-file for the context.

If FN is non-nil, it will use `teacher-students-plist-file',
otherwise use the string value of FN."
  (expand-file-name (or fn teacher-students-csv-file)
                    (teacher-school-lective-year-directory)))

(defun teacher-students--read-plist-file (&optional fn)
  "Loads into memory the students data.

It reads the data in `teacher-students-plist-file'.

When the optional parameter FN is non-nil it will try to load the
data from that file."
  (let* ((eld (expand-file-name (or fn (teacher-students-plist-file))))
         (data (if (file-exists-p eld)
                   (read-file eld)
                 (error teacher-students-eld-file-not-exists-msg eld))))
    (if (string-empty-p data)
        (error teacher-students-csv-no-data-msg eld)
      (read data))))
;; (teacher-students--read-plist-file)

(defun teacher-students--read-plist (&optional fn)
  "Sets the value of `teacher-student--plist' from file.

Normaly it loads the value from the file `teacher-students-plist-file'.

When the optional parameter FN is non-nil it will try to load the
data from that file."
  (let ((eld (or fn teacher-students-plist-file)))
    (setq teacher-students--plist (teacher-students--read-plist-file eld))))
;; (teacher-students--read-plist)

(defun teacher-students--write-plist (&optional fn data)
  "Writes the value of `teacher-students--plist' into a file-exists-p

Normaly it will write to the file `teacher-students-plist-file',
unless the optional parameter FN is non-nil, in such case, it will try
to write the value to that file-exists-p

It also will try to write the data in `teacher-students--plist',
unless the optional parameter DATA is non-nil, in such case, it will
try to write that data."
  (let ((eld (or fn (expand-file-name teacher-students-plist-file
                                      (teacher-school-lective-year-directory))))
        (students (or data teacher-students--plist)))
    (save-excursion
      (with-temp-buffer
        (insert (concat
                 ";;; " (file-name-nondirectory eld) " -*- mode:lisp-data -*-\n"
                 (object-print teacher-students--plist) "\n"))
        (write-file eld)
        (kill-buffer)))))
;; (teacher-students--write-plist)

(defun teacher-students-capture-student--record (&optional id gr rn ln fn ml)
  "Generates a new record for the `teacher-students--plist'.

If any of the optional parameters are nil (ommited), it will prompt
for each.

  ID - stands for the student's ID.
  GR - stands for the student's group.
  RN - stands for the student's roll number.
  LN - stands for the student's last name.
  FN - stands for the student's first name (this also includes any
       midle name).
  ML - Stands for the student's email address.

GR, RN and ML could be empty, only not nil.  But ID, LN and FN
could not be empty, nor nil.

If any of those are passed as nil, a prompt will ask for a value."
  ;; (message "%s | %s | %s | %s | %s | %s |"
           ;; (object-print id)
           ;; (object-print gr)
           ;; (object-print rn)
           ;; (object-print ln)
           ;; (object-print fn)
           ;; (object-print ml))
  (let ((_id (or id (read-string teacher-students-id-prompt)))
        (_gr (or gr (read-string teacher-students-gr-prompt)))
        (_rn (or rn (read-string teacher-students-rn-prompt)))
        (_ln (or ln (read-string teacher-students-ln-prompt)))
        (_fn (or fn (read-string teacher-students-fn-prompt)))
        (_ml (or ml (read-string teacher-students-ml-prompt))))
    (if (or (string-empty-p _id)
              (string-empty-p _ln)
              (string-empty-p _fn))
      ;; These three fields must be 
      (error teacher-students-csv-insufficient-data-msg
             (nth (plist-get teacher-students--parameters-index :id)
                  teacher-students-csv-file-headers)
              (nth (plist-get teacher-students--parameters-index :ln)
                  teacher-students-csv-file-headers)
              (nth (plist-get teacher-students--parameters-index :fn)
                  teacher-students-csv-file-headers))
      (list :id (upcase _id)
            :gr (upcase _gr)
            :rn _rn
            :ln (capitalize _ln)
            :fn (capitalize _fn)
            :ml (downcase _ml)))))
;; (object-print (teacher-students-capture-student--record))

(defun teacher-students--alist->plist (data &optional hdr)
  "Generates a plist from a alist (DATA).

If the opional parameter HDR is passed, it expects it to be a
list of (at least) six strings, wich will be treated as he CSV
file columns to extract.

If any "
  ;; (message "Entering -> teacher-students--alist->plist")
  (let* ((result nil)
         (idx teacher-students--parameters-index)
         (hid (nth (plist-get idx :id) hdr))
         (hgr (nth (plist-get idx :gr) hdr))
         (hrn (nth (plist-get idx :rn) hdr))
         (hln (nth (plist-get idx :ln) hdr))
         (hfn (nth (plist-get idx :fn) hdr))
         (hml (nth (plist-get idx :ml) hdr)))
    ;; (message "%s | %s | %s | %s | %s | %s |"
             ;; (object-print hid)
             ;; (object-print hgr)
             ;; (object-print hrn)
             ;; (object-print hln)
             ;; (object-print hfn)
             ;; (object-print hml))
    (dolist (row (cdr data))
      (message " -> %s" (object-print row))
      (add-to-list 'result
                   (teacher-students-capture-student--record
                    (or (cdr (assoc hid row)) "")
                    (or (cdr (assoc hgr row)) "")
                    (or (cdr (assoc hrn row)) "")
                    (or (cdr (assoc hln row)) "")
                    (or (cdr (assoc hfn row)) "")
                    (or (cdr (assoc hml row)) ""))
                   t))
    result))

(defun teacher-students--list->plist (data)
  "Generates a plist from a list (DATA)."
  ;; (message "Entering -> teacher-students--list->plist")
  (let* ((result nil)
         (idx teacher-students--parameters-index)
         (hid (plist-get idx :id))
         (hgr (plist-get idx :gr))
         (hrn (plist-get idx :rn))
         (hln (plist-get idx :ln))
         (hfn (plist-get idx :fn))
         (hml (plist-get idx :ml)))
    (dolist (row data)
      (let ((student (teacher-students-capture-student--record
                      (or (nth hid row) "")
                      (or (nth hgr row) "")
                      (or (nth hrn row) "")
                      (or (nth hln row) "")
                      (or (nth hfn row) "")
                      (or (nth hml row) ""))))
        (add-to-list 'result student t)))
    result))

(defun teacher-students--load-csv (&optional fn headers)
  "Loads the data from a CSV file.

By default, it will try to load the file
`teacher-students-csv-file'.  If the optional parameter FN is
non-nil, that will be used as the filename to load.

By default it expects the CSV to have headers.  The expected
headers could be defined in `teacher-students-csv-file-headers'.
If you want to use different headres, just pass the list of
strings to the parameter HEADERS.

If you know that the CSV you are trying to load does'n have
headers, pass `:noheaders' as HEADERS parameter, and it will
treat the file as not having headers.  But in such case, the
columns in the CSV will be taken as if the first six ones are
exactly as defined in `teacher-students-csv-file-headers' (the
order)." ;; (setq headers nil)
  (let ((csv (or fn (teacher-students-csv-file)))
        (hdr (or (and (not (eq headers :noheaders)) headers)
                 teacher-students-csv-file-headers))
        (hdr? (or (not (eq headers :noheaders)))))
    (if (file-exists-p csv)
        (with-temp-buffer
          (insert-file csv)
          (let ((data (csv-parse-buffer hdr?)))
            (setq teacher-students--plist
                  (if hdr?
                      (teacher-students--alist->plist data hdr)
                    (teacher-students--list->plist data)))
            (kill-buffer)))
      (error teacher-students-csv-file-not-exists-msg
             csv))))
;; (teacher-students--load-csv)

(defun teacher-students-filter-only (query &optional data)
  "Filters only to the students that matches all the QUERY."
  (if (eq data :none)
      :none
    (let ((qid (plist-get query :id))
          (qgr (plist-get query :gr))
          (qrn (plist-get query :rn))
          (qfn (plist-get query :fn))
          (qln (plist-get query :ln))
          (qml (plist-get query :ml))
          (students (or data teacher-students--plist))
          (return '())
          (x nil))
      (dolist (s students)
        (let ((did (plist-get s :id))
              (dgr (plist-get s :gr))
              (drn (plist-get s :rn))
              (dfn (plist-get s :fn))
              (dln (plist-get s :ln))
              (dml (plist-get s :ml)))
          (when (and (if (and qid did) (string-match-p qid did) t)
                     (if (and qgr dgr) (string-match-p qgr dgr) t)
                     (if (and qrn drn) (string-equal   qrn drn) t)
                     (if (and qln dln) (string-match-p qln dln) t)
                     (if (and qfn dfn) (string-match-p qfn dfn) t)
                     (if (and qml dml) (string-match-p qml dml) t))
            (add-to-list 'return s t))))
      (or return :none))))
;; (teacher-students-select-student (teacher-students-filter-only '(:fn "jos[ée] " :ln "g.*r")))

(defun teacher-students-filter-any (query &optional data)
  "Filters to the studenst that matches any of the QUERY."
  (if (eq data :none)
      :none
    (let ((students (or data teacher-students--plist))
          (return '()))
      (dolist (s students)
        (dolist (f '(:id :fn :ln :gr :rn :ml))
          (let ((q (plist-get query f))
                (d (plist-get s f)))
            (when (and q d (string-match-p q d))
              (add-to-list 'return s t)))))
      (or return :none))))
;; (teacher-students-select-student (teacher-students-filter-any '(:fn "Jos[ée] " :ln "g.*r")))

(defun teacher-students-format-query (&optional data)
  "Formats the DATA for selecting with vertico."
  (if (eq data :none)
      '()
    (let ((students (or  data teacher-students--plist))
          (result nil))
      (dolist (s students)
        (add-to-list 'result
                     (format "%2s | %2s | %-30s | %-30s | %-30s | %s"
                             (or (plist-get s :gr) "")
                             (or (plist-get s :rn) "")
                             (or (plist-get s :ln) "")
                             (or (plist-get s :fn) "")
                             (or (plist-get s :ml) "")
                             (or (plist-get s :id) ""))
                     t))
      result)))
;; (teacher-students-format-query `((:id "X" :gr "2C" :rn "1" :ln "Barocio" :fn "Alejandro" :ml ,user-mail-address)))

(defun teacher-students-select-student (&optional data)
  (let ((s (split-string
            (completing-read "Elige un estudiante: "
                             (teacher-students-format-query data))
            " *| *")))
    (car (teacher-students-filter-any `(:id ,(nth 5 s))
                                      data))))
;; (teacher-students-select-student `((:id "X" :gr "2C" :rn "1" :ln "Barocio" :fn "Alejandro" :ml ,user-mail-address)))

(defun teacher-students-select-students (&optional data)
  "Selects multiple students."
  (let ((ss (completing-read-multiple
             "Elige estudiantes: "
             (teacher-students-format-query data))))
    (mapcar (lambda (s)
              (teacher-students-filter-any
               `(:id ,(nth 5 (split-string s " *| *")))
               data))
            ss)))
;; (teacher-students-select-students `((:id "X" :gr "2C" :rn "1" :ln "Barocio" :fn "Alejandro" :ml ,user-mail-address) (:id "Y" :gr "2C" :rn "2" :ln "Barocio" :fn "Samuel" :ml nil)))

(defun teacher-students-select-student-field (field &optional data)
  "Selects a student and return the value of FIELD."
  (let* ((d (or data teacher-students--plist))
         (id (nth 5 (split-string
                     (completing-read
                      "Elige un estudiante: "
                      (teacher-students-format-query d))
                     " *| *")))
         (s (teacher-students-filter-any `(:id ,id) d)))
    ;; (message "id -> %s" (object-print id))
    ;; (message "s -> %s" (object-print s))
    (plist-get (car s) field)))
;; (teacher-students-select-student-field :ml `((:id "X" :gr "2C" :rn "1" :ln "Barocio" :fn "Alejandro" :ml ,user-mail-address)))

;; (defun teacher-students-select-student-sex (&optional data)
;;   (substring (students-select-student-field :id data) 10 11))
;; (teacher-students-select-student-sex)

(defun teacher-students-get-student-file (&optional id data)
  "Returns the path to the student's file.

If ID is non-nil it will use it as the student's ID; if ID is
nil, it will ask for a student to select."
  (let* ((dd (or data teacher-students--plist))
         (_id (or id (teacher-students-select-student-field :id dd))))
    (expand-file-name (format "students/%s.org" _id) (teacher-school-directory))))
;; (teacher-students-get-student-file nil `((:id "X" :gr "2C" :rn "1" :ln "Barocio" :fn "Alejandro" :ml ,user-mail-address)))

(defun teacher-students-get-student-directory (&optional id data)
  "Returns the path to the student's directory.

If ID is non-nil it will use it as the student's ID; if ID is
nil, it will ask for a student to select."
  (let* ((dd (or data teacher-students--plist))
         (_id (or id (teacher-students-select-student-field :id dd))))
    (expand-file-name (format "students/%s/" _id) (teacher-school-directory))))
;; (teacher-students-get-student-directory nil `((:id "X" :gr "2C" :rn "1" :ln "Barocio" :fn "Alejandro" :ml ,user-mail-address)))

;;;###autoload
(defun teacher-students--init ()
  "Initalize the student data and structure."
  (teacher--create-directory-no-prompt
   (expand-file-name "students/" (teacher-school-directory)))
  (if (file-exists-p (teacher-students-plist-file))
      (teacher-students--read-plist)
    (teacher-students--load-csv)))

;;; Commands

;;;###autoload
(defun teacher-students-select-compose-mail (&optional data)
  (interactive)
(let* ((dd (or data teacher-students--plist))
       (s (teacher-students-select-student dd))
       (user-mail-address teacher-email)
       (user-full-name teacher-name)
       (mail-default-reply-to teacher-email))
  (compose-mail 
   (format "\"%s %s\" <%s>"
           (plist-get s :fn)
           (plist-get s :ln)
           (plist-get s :ml)))))
;; (teacher-students-select-compose-mail `((:id "X" :gr "2C" :rn "1" :ln "Barocio" :fn "Alejandro" :ml ,user-mail-address)))

(provide 'teacher-students)
;;; teacher-students.el ends here
