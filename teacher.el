;;; teacher.el --- A teacher information management package.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Keywords: convenience, extensions, files, data, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;;; Constants
(defconst teacher-version "0.1.0" "Version number of teacher.el")

;;; Variables

(defgroup teacher '()
  "Teacher configuration for data managing."
  :tag "Teacher"
  :version "28.1"
  :package-version "0.1.0"
  :prefix "teacher"
  :group nil)

(defcustom teacher-name user-full-name
  "The teacher's full name.

By default it is set to `user-full-name'."
  :type 'string
  :group 'teacher)

(defcustom teacher-email user-mail-address
  "The teacher's email.

By default it is set to `user-mail-address'."
  :type 'string
  :group 'teacher)

(defcustom teacher-swivel-month 8
  "The month in which the lective year starts.

By default it is set to 8 (august)."
  :type 'natnum
  :group 'teacher)

(defcustom teacher-lective-year-name "lective year"
  "The string to use to display the lective year name.

By default it is set to 'lective year'.  This is here to
customize to your preferred language."
  :type 'string
  :group 'teacher)

(defcustom teacher-directory (file-name-as-directory "~/Teacher")
  "The directory to use to store all the data."
  :type 'directory
  :group 'teacher)

;;; Functions

(defun teacher-get-lective-year ()
  "Returns a string with the current lective year.

This functions depends on the value of `teacher-swivel-moth'."
  (let ((cy (string-to-number (format-time-string "%Y")))
        (cm (string-to-number (format-time-string "%m"))))
    (if (< cm teacher-swivel-month)
        (format "%4d-%4d" (1- cy) cy)
      (format "%4d-%4d" cy (1+ cy)))))
;; (teacher--lective-year)

(defun teacher-get-prev-lective-year ()
  "Returns a string with the previous lective year.

This functions depends on the value of `teacher-swivel-moth'."
  (let ((cy (string-to-number (format-time-string "%Y")))
        (cm (string-to-number (format-time-string "%m"))))
    (if (< cm teacher-swivel-month)
        (format "%4d-%4d" (- cy 2) (- cy 1))
      (format "%4d-%4d" (- cy 1) cy))))
;; (teacher--lective-year-prev)

(defun teacher-get-next-lective-year ()
  "Returns a string with the next lective year.

This functions depends on the value of `teacher-swivel-moth'."
  (let ((cy (string-to-number (format-time-string "%Y")))
        (cm (string-to-number (format-time-string "%m"))))
    (if (< cm teacher-swivel-month)
        (format "%4d-%4d" (1+ cy) (+ cy 2))
      (format "%4d-%4d" cy (1+ cy)))))
;; (teacher--lective-year-next)

(defun teacher-from-mail ()
  "Formats the FROM in a mail with the teacher's data."
  (format "\"%s\" <%s>" teacher-name teacher-email))
;; (teacher--from-mail)

(defun teacher--create-directory-no-prompt (dir)
  "Creates the directory without asking for confirmation."
  (unless (directory-name-p dir)
    (make-directory dir t))
  (directory-name-p dir))

(defun teacher--init ()
  (teacher-school--init)
  (teacher-students--init))

;;; Commands

;;;###autoload
(defun teacher-version ()
  "Displays the version of this package."
  (interactive)
  (message "teacher.el version %s" teacher-version))

;;;###autoload
(defun teacher-who-am-i? ()
  (interactive)
  (message "%s <%s>" teacher-name teacher-email))

;;;###autoload
(defun teacher-lective-year ()
  "Displays the current lective year."
  (interactive)
  (message "%s: %s"
           (capitalize teacher-lective-year-name)
           (teacher-get-lective-year)))

;;;###autoload
(defun teacher-customize (other-window)
  "Opens the customization of the group `teacher'."
  (interactive "P")
  (customize-group 'teacher other-window))

(provide 'teacher)
;;; teacher.el ends here
